#ifndef RESULTING_Q_MATRIX_H
#define RESULTING_Q_MATRIX_H

#include <QMainWindow>

namespace Ui {
class Resulting_Q_Matrix;
}

class Resulting_Q_Matrix : public QMainWindow
{
    Q_OBJECT

public:
    explicit Resulting_Q_Matrix(QWidget *parent = 0);
    void get_current_state_and_action();
    void get_next_state();
    void calculate_Q_sa();
    float Max_NextState_AllActions();
    void iterate();
    void update_Q_matrix();
    void save_Q_matrix();
    ~Resulting_Q_Matrix();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Resulting_Q_Matrix *ui;
    typedef enum {SS = 0,
                  SM = 1,
                  MS = 2,
                  MM = 3}actions;//S => Stop, M => Move; MotorL MotorR

    typedef enum {SSCC = 0,
                  SSCF = 1,
                  SSFC = 2,
                  SSFF = 3,
                  SMCC = 4,
                  SMCF = 5,
                  SMFC = 6,
                  SMFF = 7,
                  MSCC = 8,
                  MSCF = 9,
                  MSFC = 10,
                  MSFF = 11,
                  MMCC = 12,
                  MMCF = 13,
                  MMFC = 14,
                  MMFF = 15}states;//S => Stopped, M => Moving, C => Close, F => Far; MotorL, MotorR, SensorL SensorR

    //rows=>Current states, columns=>Next states
    //                                               S S S S S S S S M M M M M M M M
    //                                               S S S S M M M M S S S S M M M M
    //                                               C C F F C C F F C C F F C C F F
    //                                               C F C F C F C F C F C F C F C F
    int current_state_next_state_stop_stop[16][16]={{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //SSCC
                                                    {0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //SSCF
                                                    {0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0}, //SSFC
                                                    {0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0}, //SSFF
                                                    {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //SMCC
                                                    {0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //SMCF
                                                    {0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0}, //SMFC
                                                    {0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0}, //SMFF
                                                    {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //MSCC
                                                    {0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //MSCF
                                                    {0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0}, //MSFC
                                                    {0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0}, //MSFF
                                                    {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //MMCC
                                                    {0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, //MMCF
                                                    {0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0}, //MMFC
                                                    {0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0}};//MMFF
    //rows=>Current states, columns=>Next states (gira a la izquierda)
    //                                               S S S S S S S S M M M M M M M M
    //                                               S S S S M M M M S S S S M M M M
    //                                               C C F F C C F F C C F F C C F F
    //                                               C F C F C F C F C F C F C F C F
    int current_state_next_state_stop_move[16][16]={{0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0}, //SSCC
                                                    {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0}, //SSCF
                                                    {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0}, //SSFC
                                                    {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0}, //SSFF
                                                    {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0}, //SMCC
                                                    {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0}, //SMCF
                                                    {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0}, //SMFC
                                                    {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0}, //SMFF
                                                    {0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0}, //MSCC
                                                    {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0}, //MSCF
                                                    {0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0}, //MSFC
                                                    {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0}, //MSFF
                                                    {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0}, //MMCC
                                                    {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0}, //MMCF
                                                    {0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0}, //MMFC
                                                    {0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0}};//MMFF
    //rows=>Current states, columns=>Next states (gira a la derecha)
    //                                               S S S S S S S S M M M M M M M M
    //                                               S S S S M M M M S S S S M M M M
    //                                               C C F F C C F F C C F F C C F F
    //                                               C F C F C F C F C F C F C F C F
    int current_state_next_state_move_stop[16][16]={{0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0}, //SSCC
                                                    {0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0}, //SSCF
                                                    {0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0}, //SSFC
                                                    {0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0}, //SSFF
                                                    {0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0}, //SMCC
                                                    {0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0}, //SMCF
                                                    {0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0}, //SMFC
                                                    {0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0}, //SMFF
                                                    {0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0}, //MSCC
                                                    {0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0}, //MSCF
                                                    {0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0}, //MSFC
                                                    {0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0}, //MSFF
                                                    {0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0}, //MMCC
                                                    {0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0}, //MMCF
                                                    {0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0}, //MMFC
                                                    {0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0}};//MMFF
    //rows=>Current states, columns=>Next states
    //                                               S S S S S S S S M M M M M M M M
    //                                               S S S S M M M M S S S S M M M M
    //                                               C C F F C C F F C C F F C C F F
    //                                               C F C F C F C F C F C F C F C F
    int current_state_next_state_move_move[16][16]={{0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0}, //SSCC
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0}, //SSCF
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0}, //SSFC
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}, //SSFF
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0}, //SMCC
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0}, //SMCF
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}, //SMFC
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0}, //SMFF
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0}, //MSCC
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}, //MSCF
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0}, //MSFC
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0}, //MSFF
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0}, //MMCC
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0}, //MMCF
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0}, //MMFC
                                                    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}};//MMFF
  //                           S   S   M   M
  //                           S   M   S   M
    int reward_matrix[16][4]={{0,  0,  0,  0  }, //SSCC
                              {0,  0,  100,0  }, //SSCF
                              {0,  100,0,  0  }, //SSFC
                              {0,  100,100,100}, //SSFF
                              {0,  0,  0,  0  }, //SMCC
                              {0,  0,  100,0  }, //SMCF
                              {0,  100,0,  0  }, //SMFC
                              {0,  50, 50, 100}, //SMFF
                              {0,  0,  0,  0  }, //MSCC
                              {0,  0,  100,0  }, //MSCF
                              {0,  100,0,  0  }, //MSFC
                              {0,  50, 50, 100}, //MSFF
                              {0,  0,  0,  0  }, //MMCC
                              {0,  0,  100,0  }, //MMCF
                              {0,  100,0,  0  }, //MMFC
                              {0,  50, 50, 100}};//MMFF

    //                           S   S   M   M
    //                           S   M   S   M
      float Q_matrix[16][4]=   {{0  ,0  ,0  ,0  }, //SSCC
                                {0,  0,  0,  0  }, //SSCF
                                {0,  0,  0,  0  }, //SSFC
                                {0,  0,  0,  0  }, //SSFF
                                {0,  0,  0,  0  }, //SMCC
                                {0,  0,  0,  0  }, //SMCF
                                {0,  0,  0,  0  }, //SMFC
                                {0,  0,  0,  0  }, //SMFF
                                {0,  0,  0,  0  }, //MSCC
                                {0,  0,  0,  0  }, //MSCF
                                {0,  0,  0,  0  }, //MSFC
                                {0,  0,  0,  0  }, //MSFF
                                {0,  0,  0,  0  }, //MMCC
                                {0,  0,  0,  0  }, //MMCF
                                {0,  0,  0,  0  }, //MMFC
                                {0,  0,  0,  1  }};//MMFF
      states current_state;
      states next_state;
      actions current_action;
};


#endif // RESULTING_Q_MATRIX_H
