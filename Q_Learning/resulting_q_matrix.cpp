#include "resulting_q_matrix.h"
#include "ui_resulting_q_matrix.h"
#include <fstream>
#include <iostream>

Resulting_Q_Matrix::Resulting_Q_Matrix(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Resulting_Q_Matrix)
{
    ui->setupUi(this);
    for (int n = 0; n < 100; n++){
        get_current_state_and_action();
        get_next_state();
    }

}

void Resulting_Q_Matrix::get_current_state_and_action()
{
    current_state = static_cast<states>(rand() % 16);
    current_action = static_cast<actions>(rand() % 4);
}

void Resulting_Q_Matrix::get_next_state()
{
    switch(current_action){
        case SS:
            for (int i =0; i < 16; i++){
                if(current_state_next_state_stop_stop[current_state][i] == 1){
                    next_state = static_cast<states>(i);
                    break;
                }
            }
            break;
        case SM:
            for (int i =0; i < 16; i++){
                if(current_state_next_state_stop_move[current_state][i] == 1){
                    next_state = static_cast<states>(i);
                    break;
                }
            }
            break;
        case MS:
            for (int i =0; i < 16; i++){
                if(current_state_next_state_move_stop[current_state][i] == 1){
                    next_state = static_cast<states>(i);
                    break;
                }
            }
            break;
        case MM:
            for (int i =0; i < 16; i++){
                if(current_state_next_state_move_move[current_state][i] == 1){
                    next_state = static_cast<states>(i);
                    break;
                }
            }
            break;
    }
}
//aceleracion y jerk
void Resulting_Q_Matrix::calculate_Q_sa()
{
    float reward = reward_matrix[current_state][current_action];
    ui->LCD_reward->display(reward);
    float max_next = 0.7 * Max_NextState_AllActions();
    ui->LCD_max->display(max_next);
    Q_matrix[current_state][current_action] = reward_matrix[current_state][current_action] + 0.7 * Max_NextState_AllActions();
}

float Resulting_Q_Matrix::Max_NextState_AllActions()
{
    float max = 0;
    for (int i = 0; i < 4; i++){
        if (Q_matrix[next_state][i] > max){
            max = Q_matrix[next_state][i];
        }
    }
    return max;
}

void Resulting_Q_Matrix::iterate()
{
    for (int i = 0; i < 3000; i++){
        get_current_state_and_action();
        get_next_state();
        calculate_Q_sa();
    }
    update_Q_matrix();
    save_Q_matrix();
}

void Resulting_Q_Matrix::update_Q_matrix()
{
    ui->Q00->display(Q_matrix[0][0]);
    ui->Q01->display(Q_matrix[0][1]);
    ui->Q02->display(Q_matrix[0][2]);
    ui->Q03->display(Q_matrix[0][3]);

    ui->Q10->display(Q_matrix[1][0]);
    ui->Q11->display(Q_matrix[1][1]);
    ui->Q12->display(Q_matrix[1][2]);
    ui->Q13->display(Q_matrix[1][3]);

    ui->Q20->display(Q_matrix[2][0]);
    ui->Q21->display(Q_matrix[2][1]);
    ui->Q22->display(Q_matrix[2][2]);
    ui->Q23->display(Q_matrix[2][3]);

    ui->Q30->display(Q_matrix[3][0]);
    ui->Q31->display(Q_matrix[3][1]);
    ui->Q32->display(Q_matrix[3][2]);
    ui->Q33->display(Q_matrix[3][3]);

    ui->Q40->display(Q_matrix[4][0]);
    ui->Q41->display(Q_matrix[4][1]);
    ui->Q42->display(Q_matrix[4][2]);
    ui->Q43->display(Q_matrix[4][3]);

    ui->Q50->display(Q_matrix[5][0]);
    ui->Q51->display(Q_matrix[5][1]);
    ui->Q52->display(Q_matrix[5][2]);
    ui->Q53->display(Q_matrix[5][3]);

    ui->Q60->display(Q_matrix[6][0]);
    ui->Q61->display(Q_matrix[6][1]);
    ui->Q62->display(Q_matrix[6][2]);
    ui->Q63->display(Q_matrix[6][3]);

    ui->Q70->display(Q_matrix[7][0]);
    ui->Q71->display(Q_matrix[7][1]);
    ui->Q72->display(Q_matrix[7][2]);
    ui->Q73->display(Q_matrix[7][3]);

    ui->Q80->display(Q_matrix[8][0]);
    ui->Q81->display(Q_matrix[8][1]);
    ui->Q82->display(Q_matrix[8][2]);
    ui->Q83->display(Q_matrix[8][3]);

    ui->Q90->display(Q_matrix[9][0]);
    ui->Q91->display(Q_matrix[9][1]);
    ui->Q92->display(Q_matrix[9][2]);
    ui->Q93->display(Q_matrix[9][3]);

    ui->Q100->display(Q_matrix[10][0]);
    ui->Q101->display(Q_matrix[10][1]);
    ui->Q102->display(Q_matrix[10][2]);
    ui->Q103->display(Q_matrix[10][3]);

    ui->Q110->display(Q_matrix[11][0]);
    ui->Q111->display(Q_matrix[11][1]);
    ui->Q112->display(Q_matrix[11][2]);
    ui->Q113->display(Q_matrix[11][3]);

    ui->Q120->display(Q_matrix[12][0]);
    ui->Q121->display(Q_matrix[12][1]);
    ui->Q122->display(Q_matrix[12][2]);
    ui->Q123->display(Q_matrix[12][3]);

    ui->Q130->display(Q_matrix[13][0]);
    ui->Q131->display(Q_matrix[13][1]);
    ui->Q132->display(Q_matrix[13][2]);
    ui->Q133->display(Q_matrix[13][3]);

    ui->Q140->display(Q_matrix[14][0]);
    ui->Q141->display(Q_matrix[14][1]);
    ui->Q142->display(Q_matrix[14][2]);
    ui->Q143->display(Q_matrix[14][3]);

    ui->Q150->display(Q_matrix[15][0]);
    ui->Q151->display(Q_matrix[15][1]);
    ui->Q152->display(Q_matrix[15][2]);
    ui->Q153->display(Q_matrix[15][3]);

    ui->LCD_current_state->display(current_state);
    ui->LCD_Current_action->display(current_action);
    ui->LCD_Next_state->display(next_state);
}

void Resulting_Q_Matrix::save_Q_matrix()
{
    using namespace std;
    ofstream file_for_arduino ("/home/alejandro/Qt_Projects/Q_matrix.txt");
    if (file_for_arduino.is_open()){
        file_for_arduino << "int Q_matrix[16][4]={\n";
        for (int i = 0; i < 16; i++){
            file_for_arduino << "\t\t{";
            for (int j = 0; j < 4; j++){
                if (j < 3) {file_for_arduino << (int)Q_matrix[i][j] << ",";}
                else {file_for_arduino << (int)Q_matrix[i][j];}
            }
            if (i < 15){file_for_arduino << "},\n";}
            else {file_for_arduino << "}\n";}
        }
    }
    file_for_arduino << "\t\t};";
    file_for_arduino.close();
}

Resulting_Q_Matrix::~Resulting_Q_Matrix()
{
    delete ui;
}

void Resulting_Q_Matrix::on_pushButton_clicked()
{
    iterate();
}
