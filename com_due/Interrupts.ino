long encodL1 = 0;
long encodL2 = 0;
long encodR1 = 0;
long encodR2 = 0;

void encoderL1(){
  encodL1++;
}

void encoderL2(){
  encodL2++;
}

void encoderR1(){
  encodR1++;
}

void encoderR2(){
  encodR2++;
}

void clear_encoders(){
  encodL1 = 0;
  encodL2 = 0;
  encodR1 = 0;
  encodR2 = 0;
}
