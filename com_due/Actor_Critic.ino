#define gamma 0.2
#define beta 0.2
#define alpha 1.0
#define time_constant 0.25

#define a1 0.3
#define a2 0.3
#define a3 0.1
#define a4 0.1
#define a5 0.6
#define a6 0.4
#define a7 0.05
#define a8 0.3
#define a9 0.3

float VL = 0.0;
float VR = 0.0;

float obtain_reward(float velocityLeft, float velocityRight, float jerkLeft, float jerkRight, float dL, float dLF, float dF, float dRF, float dR){
  float Reward = a1 * 1.0/(1.0 + abs(velocityLeft - 0.1))
                +a2 * 1.0/(1.0 + abs(velocityRight - 0.1))
                -a3 * abs(jerkLeft)
                -a4 * abs(jerkRight)
                + 0.0 * a5 * 1.0 / (1.0 + (abs(dL - dR)))
                + 0.0 * a6 * 1.0 / (1.0 + (abs(dLF - dRF))) 
                + 0.0 * a7 * 1.0 / (1.0 + (abs(dF - 50.0)))
                + 0.0 * a8 * 1.0 / (1.0 + (abs(dL - 20.0)))
                + 0.0 * a9 * 1.0 / (1.0 + (abs(dR - 20.0)));

  #if DEBUG_ACTOR_CRITIC_REWARD
    Serial.print("Reward: "); Serial.println(Reward);
  #endif
  
  return Reward; 
}

void draw_actions(float velocityLeft, float velocityRight, float jerkLeft, float jerkRight, float dL, float dLF, float dF, float dRF, float dR){
  VL =   0.0 * ac_parameters.w[0] * exp(-(sq(dL - 20.0))/(2.0*sq(3.0)))
       + 0.0 * ac_parameters.w[1] * exp(-(sq(dLF - 40.0))/(2.0*sq(3.0)))
       + 0.0 * ac_parameters.w[2] * exp(-(sq((1.0/(dF+0.01)) - 0.0))/(2*sq(3.0)))
       +ac_parameters.w[3] * (1.0 - (velocityLeft * 10.0)) //velocidad medida en m/s
       +ac_parameters.w[4] * exp(-(sq(jerkLeft - 0.0))/(2.0*sq(3.0)))
       + 0.0 * ac_parameters.w[5] * exp(-(sq((dL - dR) - 0.0))/(2.0*sq(3.0)))
       + 0.0 * ac_parameters.w[6] * exp(-(sq((dLF - dRF) - 0.0))/(2.0*sq(3.0)));

  VR =  0.0 * ac_parameters.w[7]  * exp(-(sq(dR - 20.0))/(2.0*sq(3.0)))
       + 0.0 * ac_parameters.w[8]  * exp(-(sq(dRF - 40.0))/(2.0*sq(3.0)))
       + 0.0 * ac_parameters.w[2]  * exp(-(sq((1.0/(dF+0.01)) - 0.0))/(2*sq(3.0)))
       +ac_parameters.w[9]  * (1.0 - (velocityRight * 10.0)) //velocidad medida en m/s
       +ac_parameters.w[10] * exp(-(sq(jerkRight - 0.0))/(2.0*sq(3.0)))
       + 0.0 * ac_parameters.w[5]  * exp(-(sq((dL - dR) - 0.0))/(2.0*sq(3.0)))
       + 0.0 * ac_parameters.w[6]  * exp(-(sq((dLF - dRF) - 0.0))/(2.0*sq(3.0)));

  if (VL > 255){VL = 255;}
  if (VR > 255){VR = 255;}
}

float critic_evaluation (float velocityLeft, float velocityRight, float jerkLeft, float jerkRight, float dL, float dLF, float dF, float dRF, float dR){
  float Evaluation =  0.0 * ac_parameters.theta[0]  * exp(-(sq(dL - 20.0))/(2.0*sq(3.0)))
                     + 0.0 * ac_parameters.theta[1]  * exp(-(sq(dLF - 40.0))/(2.0*sq(3.0)))
                     + 0.0 * ac_parameters.theta[2]  * exp(-(sq((dF - 50.0))/(2*sq(10.0))))
                     +ac_parameters.theta[3]  * exp(-(sq(velocityLeft - 0.1))/(2*sq(3.0)))
                     +ac_parameters.theta[4]  * exp(-(sq(jerkLeft - 0.0))/(2.0*sq(3.0)))
                     + 0.0 * ac_parameters.theta[5]  * exp(-(sq((dL - dR) - 0.0))/(2.0*sq(2.0)))
                     + 0.0 * ac_parameters.theta[6]  * exp(-(sq((dLF - dRF) - 0.0))/(2.0*sq(2.0)))
                     + 0.0 *ac_parameters.theta[7]  * exp(-(sq(dR - 20.0))/(2.0*sq(3.0)))
                     + 0.0 * ac_parameters.theta[8]  * exp(-(sq(dRF - 40.0))/(2.0*sq(3.0)))
                     +ac_parameters.theta[9]  * exp(-(sq(velocityRight - 0.1))/(2*sq(3.0)))
                     +ac_parameters.theta[10] * exp(-(sq(jerkRight - 0.0))/(2.0*sq(3.0)));
  
  return Evaluation;
}

void update_w(){
  for (int i = 0; i < 11; i++){
      ac_parameters.w[i] = ac_parameters.w[i] + beta * ac_parameters.theta[i];  
  }
}

void update_theta(float previous_velocityLeft, float velocityLeft,
                  float previous_velocityRight, float velocityRight,
                  float previous_jerkLeft, float jerkLeft,
                  float previous_jerkRight, float jerkRight, 
                  float previous_dL, float dL,
                  float previous_dLF, float dLF,
                  float previous_dF, float dF,
                  float previous_dRF, float dRF,
                  float previous_dR, float dR, float delta)
{
    
  ac_parameters.theta[0]  = ac_parameters.theta[0]  + alpha * delta * (exp(-(sq(dL - 20.0))/(2.0*sq(3.0))) - exp(-(sq(previous_dL - 20.0))/(2.0*sq(3.0))));
  ac_parameters.theta[1]  = ac_parameters.theta[1]  + alpha * delta * (exp(-(sq(dLF - 40.0))/(2.0*sq(3.0))) - exp(-(sq(previous_dLF - 40.0))/(2.0*sq(3.0))));
  ac_parameters.theta[2]  = ac_parameters.theta[2]  + alpha * delta * (exp(-(sq(dF - 50.0))/(2.0*sq(10.0))) - exp(-(sq(previous_dF - 50))/(2.0*sq(10.0))));
  ac_parameters.theta[3]  = ac_parameters.theta[3]  + alpha * delta * (exp(-(sq(velocityLeft - 0.1))/(2.0*sq(3.0))) - exp(-(sq(previous_velocityLeft - 0.1))/(2.0*sq(3.0))));
  ac_parameters.theta[4]  = ac_parameters.theta[4]  + alpha * delta * (exp(-(sq(jerkLeft - 0.0))/(2.0*sq(3.0))) - exp(-(sq(previous_jerkLeft - 0.0))/(2.0*sq(3.0))));
  ac_parameters.theta[5]  = ac_parameters.theta[5]  + alpha * delta * (exp(-(sq((dL - dR) - 0.0))/(2.0*sq(2.0))) - exp(-(sq((previous_dL - previous_dR) - 0.0))/(2.0*sq(2.0))));
  ac_parameters.theta[6]  = ac_parameters.theta[6]  + alpha * delta * (exp(-(sq((dLF - dRF) - 0.0))/(2.0*sq(2.0))) - exp(-(sq((previous_dLF - previous_dRF) - 0.0))/(2.0*sq(2.0))));
  ac_parameters.theta[7]  = ac_parameters.theta[7]  + alpha * delta * (exp(-(sq(dR - 20.0))/(2.0*sq(3.0))) - exp(-(sq(previous_dR - 20.0))/(2.0*sq(3.0))));
  ac_parameters.theta[8]  = ac_parameters.theta[8]  + alpha * delta * (exp(-(sq(dRF - 40.0))/(2.0*sq(3.0))) - exp(-(sq(previous_dRF - 40.0))/(2.0*sq(3.0))));
  ac_parameters.theta[9]  = ac_parameters.theta[9]  + alpha * delta * (exp(-(sq(velocityRight - 0.1))/(2.0*sq(3.0))) - exp(-(sq(previous_velocityRight - 0.1))/(2.0*sq(3.0))));
  ac_parameters.theta[10] = ac_parameters.theta[10] + alpha * delta * (exp(-(sq(jerkRight - 0.0))/(2.0*sq(3.0))) - exp(-(sq(previous_jerkRight - 0.0))/(2.0*sq(3.0))));

}

void actor_critic(float previous_velocityLeft, float velocityLeft,
                  float previous_velocityRight, float velocityRight,
                  float previous_jerkLeft, float jerkLeft,
                  float previous_jerkRight, float jerkRight, 
                  float previous_dL, float dL,
                  float previous_dLF, float dLF,
                  float previous_dF, float dF,
                  float previous_dRF, float dRF,
                  float previous_dR, float dR)
{
  float r = obtain_reward(previous_velocityLeft, previous_velocityRight, 
                          previous_jerkLeft, previous_jerkRight, 
                          previous_dL, previous_dLF, previous_dF, previous_dRF, previous_dR);
  
  draw_actions(velocityLeft, velocityRight, 
               jerkLeft, jerkRight, 
               dL, dLF, dF, dRF, dR);
  set_motor_veolcity(Left, VL);
  set_motor_veolcity(Right, VR);

  float previous_eval = critic_evaluation(previous_velocityLeft, previous_velocityRight, 
                                          previous_jerkLeft, previous_jerkRight, 
                                          previous_dL, previous_dLF, previous_dF, previous_dRF, previous_dR);
                          
  float current_eval = critic_evaluation(velocityLeft, velocityRight, 
                                         jerkLeft, jerkRight, 
                                         dL, dLF, dF, dRF, dR);

  float delta = r + gamma * current_eval - previous_eval;

  update_w();

  update_theta(previous_velocityLeft,       velocityLeft,
               previous_velocityRight,      velocityRight,
               previous_jerkLeft,           jerkLeft,
               previous_jerkRight,          jerkRight, 
               previous_dL,                 dL,
               previous_dLF,                dLF,
               previous_dF,                 dF,
               previous_dRF,                dRF,
               previous_dR,                 dR, 
               delta);

  #if DEBUG_ACTOR_CRITIC_W
    Serial.println("W parameters of the policy");
    for (int i = 0; i < 11; i++){
      Serial.print(ac_parameters.w[i]); Serial.print(" ");  
    }
    Serial.println(" ");
  #endif

  #if DEBUG_ACTOR_CRITIC_THETA
    Serial.println("Theta parameters of the critic");
    for (int i = 0; i < 11; i++){
      Serial.print(ac_parameters.theta[i]); Serial.print(" ");  
    }
    Serial.println(" ");
  #endif

  #if DEBUG_ACTOR_CRITIC_REWARD
    Serial.print("Reward: "); Serial.println(r);
  #endif

  #if DEBUG_ACTOR_CRITIC_CRITIC_EVAL
    Serial.print("Current evaluation: "); Serial.println(current_eval);
    Serial.print("Previous evaluation: "); Serial.println(previous_eval);
  #endif

  #if DEBUG_ACTOR_CRITIC_CRITIC_DELTA
    Serial.print("Delta: ");Serial.println(delta);
  #endif

  #if DEBUG_ACTOR_CRITIC_CRITIC_VELOCITIES
    Serial.print("Velocities of the policy: ");Serial.print(VL);Serial.print(" ");Serial.println(VR); 
  #endif  
}

void check_termitation_conditions(float dL, float dLF, float dF, float dRF, float dR){
  #if TERMINATION_CONDITIONS_DEBUG
    Serial.print("Termination conditions: ");
    Serial.print(dL); Serial.print(" "); Serial.print(dLF); Serial.print(" "); Serial.print(dF); Serial.print(" "); Serial.print(dRF); Serial.print(" "); Serial.println(dR);
  #endif
  
  if (    dL  > 0.0 && dL  < 4.0
      ||  dLF > 0.0 && dLF < 4.0
      ||  dF  > 0.0 && dF  < 4.0
      ||  dRF > 0.0 && dRF < 4.0
      ||  dR  > 0.0 && dR  < 4.0){terminate();}
      
  {
    Serial.println("distancia corta");
    terminate();
  }
}

void terminate(){
  stop_left();
  stop_right();
  first_run = true;
  terminate_flag = true;
}

void check_emergency_stop(){
  if (Serial.read() == 's'){terminate();}  
}

void check_restart(){
  if (Serial.read() == 'r'){terminate_flag = false;}   
}






