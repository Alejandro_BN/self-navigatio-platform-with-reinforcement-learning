#include "common_defines.h"
#include "debug_defines.h"
#include <math.h>
#include <DueFlashStorage.h>

bool terminate_flag = true;

DueFlashStorage dueFlashStorage;
struct parameters{
  float w[11];
  float theta[11]; 
};
parameters ac_parameters;// create a temporary struct
parameters ac_parameters_copy;// create permanent struct

typedef enum{
  // Left-motor Right-motor Left-sensor Right-sensor
  Stopped_Stopped_Close_Close = 0,
  Stopped_Stopped_Close_Far = 1,
  Stopped_Stopped_Far_Close = 2,
  Stopped_Stopped_Far_Far = 3,
  Stopped_Moving_Close_Close = 4,
  Stopped_Moving_Close_Far = 5,
  Stopped_Moving_Far_Close = 6,
  Stopped_Moving_Far_Far = 7,
  Moving_Stopped_Close_Close = 8,
  Moving_Stopped_Close_Far = 9,
  Moving_Stopped_Far_Close = 10,
  Moving_Stopped_Far_Far = 11,
  Moving_Moving_Close_Close = 12,
  Moving_Moving_Close_Far = 13,
  Moving_Moving_Far_Close = 14,
  Moving_Moving_Far_Far = 15  
}state;

typedef enum{
  // Left-motor Right-motor
  Stop_Stop,
  Stop_Move,
  Move_Stop,
  Move_Move  
}action;

typedef enum{
  Left,
  Right
}motor;

bool first_run = true;

float prev_final_distance[5];
float prev_velocityLeft;
float prev_velocityRight;
float prev_jerkLeft;
float prev_jerkRight;

void setup() {
  init_serial_ports();
  init_encoders();
  init_motor(); 
  initialize_w_and_theta();
}

void loop() {
  main_execution();
  reset_timers();
}
