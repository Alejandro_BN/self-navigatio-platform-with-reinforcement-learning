void main_execution(){
  get_distances();
  
  //Task with 1s periodicity
  if (one_second_passed()){
    //ac_parameters.w[0] = ac_parameters.w[0] + 0.1;
    show_messages(terminate_flag);
    copy_variables_to_flash();
    check_restart();
    //ac_parameters.w[0] = ac_parameters.w[0] + 0.1;
    //Serial.println(ac_parameters.w[0]);
    //make_action(Stop_Stop);
    //make_action(Move_Move);
    //make_action(Stop_Move);
    //make_action(Move_Stop);
  }
  
  //Task with 1/4s periodicity
  if (quarter_second_passed()){
    average_distances();
    VelocityRampGenerator();
    get_movement_magnitudes(encodL1, encodR1);
    //debug_encoders();//Comentar cuando no se haya terminado de programar definitivamente

    if (!terminate_flag){
    /*//Q-Learning
      state current_state = get_state(distanceLeft, distanceRight, final_distance[1], final_distance[3]);
      action best_action = get_best_action(current_state);
      make_action(best_action);*/

    //Actor critic
      if (first_run){
        first_run = false;

        //draw_actions(velocityLeft, velocityRight, jerkLeft, jerkRight, final_distance[0], final_distance[1], final_distance[2], final_distance[3], final_distance[4]);
        draw_actions(velocityLeft, velocityRight, jerkLeft, jerkRight, 0, 0, 0, 0, 0);// es una prueba
        set_motor_veolcity(Left, VL);
        set_motor_veolcity(Right, VR);
      
        prev_velocityLeft = velocityLeft;
        prev_velocityRight = velocityRight; 

        prev_jerkLeft = jerkLeft;
        prev_jerkRight = jerkRight;

        for (int i = 0; i < 5; i++){
          prev_final_distance[i] = final_distance[i];  
        }
      }

      else {
        //actor_critic(prev_velocityLeft,       velocityLeft,
        //             prev_velocityRight,      velocityRight,
        //             prev_jerkLeft,           jerkLeft,
        //             prev_jerkRight,          jerkRight,
        //             prev_final_distance[0],  final_distance[0],
        //             prev_final_distance[1],  final_distance[1],
        //             prev_final_distance[2],  final_distance[2],
        //             prev_final_distance[3],  final_distance[3],
        //             prev_final_distance[4],  final_distance[4]);
        actor_critic(prev_velocityLeft,       velocityLeft,
                     prev_velocityRight,      velocityRight,
                     prev_jerkLeft,           jerkLeft,
                     prev_jerkRight,          jerkRight,
                     0,  0,
                     0,  0,
                     0,  0,
                     0,  0,
                     0,  0);// es una prueba
      }
      //end of actor critic
    
      prev_velocityLeft = velocityLeft;
      prev_velocityRight = velocityRight; 

      prev_jerkLeft = jerkLeft;
      prev_jerkRight = jerkRight;

      for (int i = 0; i < 5; i++){
        prev_final_distance[i] = final_distance[i];  
      }
      //check_termitation_conditions(final_distance[0], final_distance[1], final_distance[2], final_distance[3], final_distance[4]);
      check_emergency_stop();
    }
  }  


  //Task with 1/10s periodicity
  /*if (tenth_second_passed()){
    //get_movement_magnitudes(encodL1, encodR1);
    //if (!terminate_flag){
      //Q-Learning
      //state current_state = get_state(distanceLeft, distanceRight, filtered_distance[1], filtered_distance[3]);
      //action best_action = get_best_action(current_state);
      //make_action(best_action);

      check_termitation_conditions(filtered_distance[0], filtered_distance[1], filtered_distance[2], filtered_distance[3], filtered_distance[4]);
      check_emergency_stop();
    }
  }*/

}



