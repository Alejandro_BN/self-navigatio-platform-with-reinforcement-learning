
void debug_encoders(){
  #if DEBUG_ENCODER_L1
    Serial.print("L1 ");Serial.println(encodL1);
  #endif
  #if DEBUG_ENCODER_L2
    Serial.print("L2 ");Serial.println(encodL2);
  #endif  
  #if DEBUG_ENCODER_R1
    Serial.print("R1 ");Serial.println(encodR1);
  #endif
  #if DEBUG_ENCODER_R2
    Serial.print("R2 ");Serial.println(encodR2);
  #endif
}

void VelocityRampGenerator(){
  static int counter = 0;
  static int Move_SPD = 0;
  counter++;
  if (counter >= 10 && counter < 18){
    Move_SPD += 25;  
  }
  else if (counter >= 33 && counter < 41){
    Move_SPD -= 25;
  }
  else if (counter == 41){
    Move_SPD = 0;
    counter = 0;  
  }
  analogWrite(ENL, Move_SPD);
  digitalWrite(IN1L, LOW);
  digitalWrite(IN2L, HIGH);
  analogWrite(ENR, Move_SPD);
  digitalWrite(IN1R, LOW);
  digitalWrite(IN2R, HIGH);
}
