#define Move_Speed 170

void make_action(action action){
  #if DEBUG_CURRENT_ACTION
    Serial.print("Current action: ");Serial.println(action);
  #endif
  
  switch (action){
    case Stop_Stop:
      stop_left();
      stop_right();
      break;
    
    case Stop_Move:
      stop_left();
      move_right();
      break;
    
    case Move_Stop:
      move_left();
      stop_right();
      break;

    case Move_Move:
      move_left();
      move_right();
      break;
  }  
}

void move_motor(motor motor){
  #if MOVE_MOTOR_DEBUG
    Serial.print("Moving motor: ");Serial.println(motor);
  #endif
  if (motor == Left){
    analogWrite(ENL, Move_Speed);
    digitalWrite(IN1L, LOW);
    digitalWrite(IN2L, HIGH);
  }
  else{
    analogWrite(ENR, Move_Speed);
    digitalWrite(IN1R, LOW);
    digitalWrite(IN2R, HIGH);  
  }
}

void move_right(){
  move_motor(Right);  
}

void move_left(){
  move_motor(Left);  
}

void stop_motor(motor motor){
  #if STOP_MOTOR_DEBUG
    Serial.print("Stopping motor: ");Serial.println(motor);
  #endif
  if (motor == Left){
    digitalWrite(ENL, LOW);
    digitalWrite(IN1L, LOW);
    digitalWrite(IN2L, HIGH);
  }
  else{
    digitalWrite(ENR, LOW);
    digitalWrite(IN1R, LOW);
    digitalWrite(IN2R, HIGH);  
  }  
}

void stop_right(){
  stop_motor(Right);  
}

void stop_left(){
  stop_motor(Left);  
}

void set_motor_veolcity(motor motor, float velocity){
  if (motor == Left){
    analogWrite(ENL, velocity);
    digitalWrite(IN1L, LOW);
    digitalWrite(IN2L, HIGH);
  }
  else {
    analogWrite(ENR, velocity);
    digitalWrite(IN1R, LOW);
    digitalWrite(IN2R, HIGH);  
  }
}

