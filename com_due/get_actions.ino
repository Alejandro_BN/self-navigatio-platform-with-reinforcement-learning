int Q_matrix[16][4]={
//   SS  SM  MS  MM
    {163,233,114,163},//SSCC
    {233,233,333,233},//SSCF
    {233,333,233,233},//SSFC
    {233,333,333,334},//SSFF
    {163,233,114,163},//SMCC
    {233,233,333,233},//SMCF
    {233,333,233,233},//SMFC
    {233,283,283,333},//SMFF
    {163,164,114,163},//MSCC
    {233,233,333,233},//MSCF
    {233,333,233,233},//MSFC
    {233,283,283,333},//MSFF
    {163,233,114,163},//MMCC
    {233,233,333,233},//MMCF
    {233,333,233,233},//MMFC
    {233,283,283,333} //MMFF
    };

action get_best_action(state current_state){
  
  action best_action = Stop_Stop;
  int value = Q_matrix[current_state][(int)Stop_Stop];
  for (int i = 1; i < 4; i++){
    if (Q_matrix[current_state][i] > value){
      best_action = (action)i;
      value = Q_matrix[current_state][i];
    }
  }

  #if DEBUG_BEST_ACTION
    Serial.print("Best action to perform: ");
    Serial.println(best_action);
  #endif
  return best_action;
}


