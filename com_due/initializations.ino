void init_serial_ports(){
  Serial3.begin(9600);
  Serial.begin(115200);  
}

void init_encoders(){
  pinMode(encoder_L1, INPUT);
  pinMode(encoder_L2, INPUT);
  pinMode(encoder_R1, INPUT);
  pinMode(encoder_R2, INPUT);
  attachInterrupt(digitalPinToInterrupt(encoder_L1), encoderL1, RISING);
  attachInterrupt(digitalPinToInterrupt(encoder_L2), encoderL2, RISING);
  attachInterrupt(digitalPinToInterrupt(encoder_R1), encoderR1, RISING);
  attachInterrupt(digitalPinToInterrupt(encoder_R2), encoderR2, RISING);  
}

void init_motor(){
  pinMode(ENL, OUTPUT);
  pinMode(IN1L, OUTPUT);
  pinMode(IN2L, OUTPUT);
  pinMode(ENR, OUTPUT);
  pinMode(IN1R, OUTPUT);
  pinMode(IN2R, OUTPUT);
}

void initialize_w_and_theta(){
  if (dueFlashStorage.read(100) != 3){
    dueFlashStorage.write(100, 3);
    for (int i = 0; i < 11; i++){
      ac_parameters_copy.w[i] = 0.1;
      ac_parameters_copy.theta[i] = 0.1;  
    }
    byte b_copy[sizeof(ac_parameters_copy)]; // create byte array to store the struct
    memcpy(b_copy, &ac_parameters_copy, sizeof(ac_parameters_copy)); // copy the struct to the byte array
    dueFlashStorage.write(0, b_copy, sizeof(ac_parameters_copy)); // write byte array to flash
  }
  byte* b = dueFlashStorage.readAddress(0); // byte array which is read from flash at adress 0
  memcpy(&ac_parameters, b, sizeof(ac_parameters_copy)); // copy byte array to temporary struct
}
