#define wheel_radius 0.02
#define time_constant 0.25
#define pi 3.1416
#define encoder_pulses_per_wheel_spin 100.0

float distanceLeft = 0.0;
float distanceRight = 0.0;
float velocityLeft = 0.0;
float velocityRight = 0.0;
float accelerationLeft = 0.0;
float accelerationRight = 0.0;
float jerkLeft = 0.0;
float jerkRight = 0.0;

float distance_travelled(int encoder_pulses){
  float distance = 2.0*pi*wheel_radius*((float)encoder_pulses/encoder_pulses_per_wheel_spin);
  #if DEBUG_DISTANCE_TRAVELLED
    Serial.print("Distance travelled: "); Serial.println(distance);
  #endif
  return distance;  
}

float current_velocity(float distance_travelled){
  float velocity = distance_travelled/time_constant;
  #if DEBUG_CURRENT_VELOCITY
    Serial.print("Current velocity: "); Serial.println(velocity);
  #endif
  return velocity;   
}

float current_acceleration(float current_velocity, motor motor){
  switch (motor){
    case Left:
      return current_acceleration_left(current_velocity);
      break;  
    case Right:
      return current_acceleration_right(current_velocity);
      break;
  }  
}

float current_acceleration_left(float current_velocity){
  static float previous_velocity_left = 0.0;
  float acceleration = (current_velocity - previous_velocity_left)/time_constant;
  #if DEBUG_CURRENT_ACCELERATION
    Serial.print("Previous velocity: "); Serial.print(previous_velocity_left); 
    Serial.print(" Current velocity: ");Serial.print(current_velocity);
    Serial.print(" Acceleration: ");Serial.println(acceleration); 
  #endif
  previous_velocity_left = current_velocity;
  return acceleration;
}

float current_acceleration_right(float current_velocity){
  static float previous_velocity_right = 0.0;
  float acceleration = (current_velocity - previous_velocity_right)/time_constant;
  #if DEBUG_CURRENT_ACCELERATION
    Serial.print("Previous velocity: "); Serial.print(previous_velocity_right); 
    Serial.print(" Current velocity: ");Serial.print(current_velocity);
    Serial.print(" Acceleration: ");Serial.println(acceleration); 
  #endif
  previous_velocity_right = current_velocity;
  return acceleration;
}

float current_jerk(float current_acceleration, motor motor){
  switch (motor){
    case Left:
      return current_jerk_left(current_acceleration);
      break;  
    case Right:
      return current_jerk_right(current_acceleration);
      break;
  }  
}

float current_jerk_left(float current_acceleration){
  static float previous_acceleration_left = 0.0;
  float jerk = (current_acceleration - previous_acceleration_left)/time_constant;
  #if DEBUG_CURRENT_ACCELERATION
    Serial.print("Previous acceleration: "); Serial.print(previous_acceleration_left); 
    Serial.print(" Current acceleration: ");Serial.print(current_acceleration);
    Serial.print(" Jerk: ");Serial.println(jerk); 
  #endif
  previous_acceleration_left = current_acceleration;
  return jerk;
}

float current_jerk_right(float current_acceleration){
  static float previous_acceleration_right = 0.0;
  float jerk = (current_acceleration - previous_acceleration_right)/time_constant;
  #if DEBUG_CURRENT_ACCELERATION
    Serial.print("Previous acceleration: "); Serial.print(previous_acceleration_right); 
    Serial.print(" Current acceleration: ");Serial.print(current_acceleration);
    Serial.print(" Jerk: ");Serial.println(jerk); 
  #endif
  previous_acceleration_right = current_acceleration;
  return jerk;
}

void get_movement_magnitudes(int encodL1, int encodR1){
  distanceLeft = distance_travelled(encodL1);
  distanceRight = distance_travelled(encodR1);
  velocityLeft = current_velocity(distanceLeft);
  velocityRight = current_velocity(distanceRight);
  accelerationLeft = current_acceleration(velocityLeft, Left);
  accelerationRight = current_acceleration(velocityRight, Right);
  jerkLeft = current_jerk(accelerationLeft, Left);
  jerkRight = current_jerk(accelerationRight, Right);
  
  clear_encoders();
  
  #if PLOT_MOVEMENT_MAGNITUDES
    Serial.print(distanceLeft*100);Serial.print(" ");
    Serial.print(distanceRight*100);Serial.print(" ");
    Serial.print(velocityLeft*100);Serial.print(" ");
    Serial.print(velocityRight*100);Serial.print(" ");
    Serial.print(accelerationLeft*100);Serial.print(" ");
    Serial.print(accelerationRight*100);Serial.print(" ");
    Serial.print(jerkLeft*100);Serial.print(" ");
    Serial.print(jerkRight*100);Serial.println(" ");    
  #endif
}


