#define one_s_in_ms 1000
#define quarter_s_in_ms 250
#define tenth_s_in_ms 100

unsigned long previous_time_one_second = 0;
unsigned long previous_time_quarter_second = 0;
unsigned long previous_time_tenth_second = 0;
bool one_second = false;
bool quarter_second = false;
bool tenth_second = false;

bool one_second_passed(){
  if (millis() - previous_time_one_second >= one_s_in_ms){
    previous_time_one_second = millis();
    one_second = true;
  }
  return one_second;
}

bool quarter_second_passed(){
  if (millis() - previous_time_quarter_second >= quarter_s_in_ms){
    previous_time_quarter_second = millis();
    quarter_second = true;
  }
  return quarter_second;
}

bool tenth_second_passed(){
  if (millis() - previous_time_tenth_second >= tenth_s_in_ms){
    previous_time_tenth_second = millis();
    tenth_second = true;
  }
  return tenth_second;
}

void reset_timers(){
  if (one_second){one_second = false;}
  if (quarter_second){quarter_second = false;}
  if (tenth_second){tenth_second = false;}  
}


