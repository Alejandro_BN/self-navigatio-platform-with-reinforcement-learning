
state get_state(float distance_travelled_L, float distance_travelled_R, float distance_sensor_L, float distance_sensor_R){
  int current_state = 0;
  if(distance_travelled_L > 0.005){current_state+=8;}
  if(distance_travelled_R > 0.005){current_state+=4;}
  if(distance_sensor_L > 40){current_state+=2;}
  if(distance_sensor_R > 40){current_state+=1;}  
  #if DEBUG_CURRENT_STATE
    Serial.print("Current state: "); Serial.println(current_state);
  #endif
  //clear_encoders();
  return (state)current_state;    
}

