#define YES 1
#define NO 0

#define DISTANCES_DEBUG NO
#define DISTANCES_PLOT NO
#define PLOT_FINAL_DISTANCES YES

#define DEBUG_ENCODER_L1 NO
#define DEBUG_ENCODER_L2 NO
#define DEBUG_ENCODER_R1 NO
#define DEBUG_ENCODER_R2 NO

#define DEBUG_DISTANCE_TRAVELLED NO
#define DEBUG_CURRENT_VELOCITY YES
#define DEBUG_CURRENT_ACCELERATION NO
#define PLOT_MOVEMENT_MAGNITUDES NO

#define DEBUG_CURRENT_STATE NO
#define DEBUG_BEST_ACTION NO

#define DEBUG_CURRENT_ACTION NO

#define MOVE_MOTOR_DEBUG NO
#define STOP_MOTOR_DEBUG NO 

#define DEBUG_ACTOR_CRITIC_W YES
#define DEBUG_ACTOR_CRITIC_THETA YES
#define DEBUG_ACTOR_CRITIC_REWARD YES
#define DEBUG_ACTOR_CRITIC_CRITIC_EVAL YES
#define DEBUG_ACTOR_CRITIC_CRITIC_DELTA YES
#define DEBUG_ACTOR_CRITIC_CRITIC_VELOCITIES YES
#define TERMINATION_CONDITIONS_DEBUG NO
