#define K_filter1 0.1
#define X_filter2 11
#define a_filter3 0.3
#define b0 0.3618
#define b1 0.7237
#define b2 0.3618
#define a1 -0.2634
#define a2 -0.1839
#define kalman_r 100.0
#define kalman_q 10.0
float kalman_K = 0;
float kalman_p = 1024.0;

char received[24];
char character[4];
int init_character = 0;
int end_character = 0;

float sensor_distance[5] =  {0.0, 0.0, 0.0, 0.0, 0.0};
float sensor_distance_1[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
float sensor_distance_2[5] = {0.0, 0.0, 0.0, 0.0, 0.0};

float filtered_distance[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
float filtered_distance_1[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
float filtered_distance_2[5] = {0.0, 0.0, 0.0, 0.0, 0.0};

int number_of_samples = 0;
float stored_distances[5][100];
float final_distance[5] = {0.0, 0.0, 0.0, 0.0, 0.0};

void get_distances(){
  if (Serial3.available() > 23){
    while(Serial3.available()){
       Serial3.read();
    }
  }
  
  if (Serial3.available() == 23){
    int i = 0;
    while (Serial3.available()){
      received[i] = Serial3.read();
      i++;
    }
    
    for (int i = 0; i < 3; i++){character[i] = received[i];}
    init_character = atoi(character);
    for (int i = 18 ; i < 21; i++){character[i-18] = received[i];}
    end_character = atoi(character);

    if (init_character == 255 && end_character == 128){
      for (int i = 1; i < 6; i++){
        for (int j = 0; j < 3; j++){character[j] = received[3*i + j];}
        sensor_distance[i-1] = atoi(character);
      }
    }
    filter_distances();
    store_distances();

    number_of_samples++;

    #if DISTANCES_DEBUG
      Serial.print("Measured distances: ");
      for (int i = 0; i < 5; i++){
        Serial.print(sensor_distance[i]); Serial.print(" ");  
      }
      Serial.println(" ");
      Serial.print("Filtered distances: ");
      for (int i = 0; i < 5; i++){
        Serial.print(filtered_distance[i]); Serial.print(" ");  
      }
      Serial.println(" ");
      Serial.print("averaged distances: ");
      for (int i = 0; i < 5; i++){
        Serial.print(final_distance[i]); Serial.print(" ");  
      }
      Serial.println(" ");Serial.println(" ");
    #endif
    #if DISTANCES_PLOT
      Serial.print(sensor_distance[0]);
      Serial.print(" ");
      Serial.print(filtered_distance[0]);
      Serial.print(" ");
      Serial.print(final_distance[0]);
      Serial.println(" ");
    #endif
  }  
}

void filter_distances(){
  for (int i = 0; i < 5; i++){
    low_pass_filter1(i);
    //low_pass_filter2(i);
    //low_pass_filter3(i);  
    //second_order_low_pass_filter(i);
    //kalman_filter(i);
  }
}

void low_pass_filter1(int i){
  filtered_distance[i] = filtered_distance[i] + K_filter1 * (sensor_distance[i] - filtered_distance[i]);
}

void low_pass_filter2(int i){
  filtered_distance[i] = (X_filter2 * filtered_distance[i] + sensor_distance[i])/(X_filter2 + 1);
}

void low_pass_filter3(int i){
  filtered_distance[i] = (1 - a_filter3) * sensor_distance[i] + a_filter3 * filtered_distance[i];  
}

void second_order_low_pass_filter(int i){
  filtered_distance[i] = b0*sensor_distance[i] + b1*sensor_distance_1[i] + b2*sensor_distance_2[i] + a1*filtered_distance_1[i] + a2*filtered_distance_2[i];
  update_previous_values(i); 
}

void update_previous_values(int i){
  sensor_distance_2[i] = sensor_distance_1[i];
  sensor_distance_1[i] = sensor_distance[i];
  filtered_distance_2[i] = filtered_distance_1[i];
  filtered_distance_1[i] = filtered_distance[i];
}

void kalman_filter(int i){
  kalman_p = kalman_p + kalman_q;
  kalman_K = kalman_p/(kalman_p + kalman_r);
  filtered_distance[i] = filtered_distance[i] + kalman_K * (sensor_distance[i] - filtered_distance[i]);
  kalman_p = (1 - kalman_K) * kalman_p; 
}

void store_distances(){
  for (int i = 0; i < 5; i++){
    store_distance(i);  
  }
}

void store_distance(int i){
  stored_distances[i][number_of_samples] = filtered_distance[i];
}

void average_distances(){
  for (int i = 0; i < 5; i++){
    average_distance(i);  
  }
  number_of_samples = 0;
  #if PLOT_FINAL_DISTANCES
    for (int i = 0; i < 5; i++){
      Serial.print(final_distance[i]);Serial.print(" ");  
    }
    Serial.println(" ");
  #endif
}

void average_distance(int i){
  float sum = sum_of_distances(i);;
  final_distance[i] = sum / (float)number_of_samples;
}

float sum_of_distances(int i){
  float sum = 0.0;
  for (int j = 0; j < number_of_samples; j++){
    sum = sum + stored_distances[i][j];  
  }
  return sum; 
}


