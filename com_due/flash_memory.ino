void copy_variables_to_flash(){
  byte b2_ac_parameters[sizeof(ac_parameters_copy)];
  memcpy(b2_ac_parameters, &ac_parameters, sizeof(ac_parameters_copy));  
  dueFlashStorage.write(0, b2_ac_parameters, sizeof(ac_parameters_copy));
}
