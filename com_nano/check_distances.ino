void get_distances(){
  sensor_distances[0] = left.distanceRead(CM);
  sensor_distances[1] = left_front.distanceRead(CM);
  sensor_distances[2] = front.distanceRead(CM);
  sensor_distances[3] = right_front.distanceRead(CM);
  sensor_distances[4] = right.distanceRead(CM);  
}

String adapt_distance(int distance){
  String distances;
  
  if (distance < 10){
    distances = zero + zero + distance;  
  }
  else if (distance < 100){
    distances = zero + distance;
  }  

  else {distances += distance;}

  return distances;
}

void compose_distances_string(){
  distances = init_char;
  for (int i = 0; i < 5; i++){
    distances += adapt_distance(sensor_distances[i]);  
  }
  distances += end_char;
}
