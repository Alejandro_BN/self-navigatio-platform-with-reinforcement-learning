#include <Ultrasonic.h>

Ultrasonic left(2,3); // pins 2 (output): Trig_pin , pin 3(input): Echo_pin
Ultrasonic left_front(8,9);// , // // //
Ultrasonic front(6,7);
Ultrasonic right_front(4,5);
Ultrasonic right(10,11);

int distance_left = 0;
String d_left;
int distance_left_front = 0;
String d_left_front;
int distance_front = 0;
String d_front;
int distance_right_front = 0;
String d_right_front;
int distance_right = 0;
String d_right;

int sensor_distances [5];

String distances;
String zero;
String init_char;
String end_char;

void setup() {
  Serial.begin(9600);
  zero += 0;
  init_char += 255;
  end_char += 128;
}

void loop() {
  get_distances();
  compose_distances_string();
  Serial.println(distances);
  //para debug y por poco tiempo
  /*if (Serial.available()){
    Serial.print(Serial.read());  
  }*/
  delay(10);
}
