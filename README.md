Self navigating with based on reinforcemet learning

This repository is for documenting the advances in a project started in class but that is still far from ending

* The code until now is using reinforcement Q-learning and natural actor-critic but still without decent results
* Version 0.1

Setup

* The setup is just a movile base with five sonar sensor covering 180� in the top part of the robot and 5 luminance sensors for using the base as a line tracker
* The code is entirely based on arduino
* The main board is an Arduino Due

This code has been created and will be maintained by Alejandro Beltr�n Nova (abeltrannova@gmail.com) with the invaluable asistance of (Ramiro S. Castro Rios ramiro.saito.castro@estudiant.upc.edu) and Jiaqiang Yao (jiaqiang.yao@estudiant.upc.edu )

When some result will be got you will be able to se some videos at: https://www.youtube.com/user/spammeron/videos?view=0&shelf_id=0&sort=dd&view_as=subscriber
